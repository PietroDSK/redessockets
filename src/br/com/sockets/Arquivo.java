/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.sockets;

import java.io.Serializable;
import java.util.Date;
public class Arquivo implements Serializable {
    
    private static final long serialVersionUID = 1l;
    
    private String nome;
    private byte[] conteudo;
    private transient long tamanhoKB;
    private transient Date dataHoraUpload;
    private transient String ipDestino;
    private transient String portaDestino;
    private String diretorioDestino;

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the conteudo
     */
    public byte[] getConteudo() {
        return conteudo;
    }

    /**
     * @param conteudo the conteudo to set
     */
    public void setConteudo(byte[] conteudo) {
        this.conteudo = conteudo;
    }

    /**
     * @return the tamanhoKB
     */
    public long getTamanhoKB() {
        return tamanhoKB;
    }

    /**
     * @param tamanhoKB the tamanhoKB to set
     */
    public void setTamanhoKB(long tamanhoKB) {
        this.tamanhoKB = tamanhoKB;
    }

    /**
     * @return the dataHoraUpload
     */
    public Date getDataHoraUpload() {
        return dataHoraUpload;
    }

    /**
     * @param dataHoraUpload the dataHoraUpload to set
     */
    public void setDataHoraUpload(Date dataHoraUpload) {
        this.dataHoraUpload = dataHoraUpload;
    }

    /**
     * @return the ipDestino
     */
    public String getIpDestino() {
        return ipDestino;
    }

    /**
     * @param ipDestino the ipDestino to set
     */
    public void setIpDestino(String ipDestino) {
        this.ipDestino = ipDestino;
    }

    /**
     * @return the portaDestino
     */
    public String getPortaDestino() {
        return portaDestino;
    }

    /**
     * @param portaDestino the portaDestino to set
     */
    public void setPortaDestino(String portaDestino) {
        this.portaDestino = portaDestino;
    }

    /**
     * @return the diretorioDestino
     */
    public String getDiretorioDestino() {
        return diretorioDestino;
    }

    /**
     * @param diretorioDestino the diretorioDestino to set
     */
    public void setDiretorioDestino(String diretorioDestino) {
        this.diretorioDestino = diretorioDestino;
    }
}
